# ATAC-seq / ChIP-seq / Open Chromatin pipeline

**This repository contains the Open Chromatin pipeline from the Zaugg lab.**

The default master branch hosts the code for the stable version of the pipeline, while the dev branch hosts the development version.

**If you want to use the pipeline, please follow the following instructions:**

1. Clone the repository and switch to the branch you want to use. This will almost always be the "stable" version from the master branch, in which case nothing has to be done - you already have, by default, the master branch of the repository.
  `git clone git@git.embl.de:carnold/ATACSeq_Snakemake.git`. To see all available branches, type "git branch -a". You should see that the master branch is already the currently active branch
2. Use the `examples/templates` folder to quickly get started with examples for the genome of your choice and prepare your dataset. In addition, you can check the `examples/input` folder.
3. Create a new conda / mamba environment in which you install the newest version of snakemake. Follow the snakemake installation instructions from the official website. Nothing else is needed, as our pipeline uses pre-build Singularity containers that contain all required software.
4. Download the additional annotation and resource files that are required - see the config.yaml and also the separate instructions here below. You may also use your own versions, of course, but we do have and can provide example files for your genome assembly.
6. Run the pipeline by executing snakemake - either use the wrapper scripts we provide or run snakemake manually on your own. See the file `Documentation.pdf` in the doc folder for more details.


**For parameter explanation, see the explanations in the config yaml files.** There are some important changes that were introduced recently. Make sure to read the config.yaml and ask Christian in case of open questions. In short, some parameters and parts of the pipeline have been deprecated (Encode peak calling), default paramters have changed (e.g., no RSS adjustment is done anymore by default, but this can be changed).


