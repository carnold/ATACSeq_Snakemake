
start.time  <-  Sys.time()
library(checkmate)
assertClass(snakemake, "Snakemake")
snakemake@source("functions.R")


########################################################################
# SAVE SNAKEMAKE S4 OBJECT THAT IS PASSED ALONG FOR DEBUGGING PURPOSES #
########################################################################

# Use the following line to load the Snakemake object to manually rerun this script (e.g., for debugging purposes)
# Replace {outputFolder} correspondingly.
# snakemake = readRDS("{outputFolder}/LOGS_AND_BENCHMARKS/1.annotatePeaks.R.rds")
createDebugFile(snakemake)


initFunctionsScript(packagesReq = NULL, minRVersion = "3.1.0", warningsLevel = 1, disableScientificNotation = TRUE)
checkAndLoadPackages(c("futile.logger", "checkmate", "stats", "tidyverse", "tools"), verbose = FALSE)


#########################
# ADDITIONAL PARAMETERS #
#########################

par.l = list()
par.l$log_minlevel = "INFO"
par.l$ggplot_binwidth = 1
par.l$ggplot_maxBreaks = 50
par.l$pdf_width = 12
par.l$pdf_height = 6
par.l$ggplot_axis.title.size = 16
par.l$ggplot_axis.text.x = 8
par.l$ggplot_axis.text.y = 12
par.l$ggplot_axis.col = "black"
par.l$ggplot_hist.col = "black"

par.l$verbose = TRUE
par.l$log_minlevel = "INFO"

allFiles     = as.character(snakemake@input)
par.l$cutoff           = as.numeric(snakemake@params$FL_distr_cutoff)
par.l$file_outputPDF   = as.character(snakemake@output$pdf)
par.l$file_log         = as.character(snakemake@log)
par.l$file_outputRData = as.character(snakemake@output$data)

assertIntegerish(par.l$cutoff, lower = 1)
assertDirectory(dirname(par.l$file_outputPDF), access = "w")
assertDirectory(dirname(par.l$file_log), access = "w")

######################
# FINAL PREPARATIONS #
######################
startLogger(par.l$file_log, par.l$log_minlevel, removeOldLog = TRUE)
printParametersLog(par.l)


##################################
# Plot distribution of fragments #
##################################

pdf(par.l$file_outputPDF, width = par.l$pdf_width, height = par.l$pdf_height)

results.l = list()

for (fileCur in allFiles) {
  
  assertFileExists(fileCur, access = "r")
  
  ##########################
  # READ AND PROCESS INPUT #
  ##########################

  tbl.df <- read_tsv(fileCur, col_names = FALSE, col_types = list(
      col_skip(), # "CHR,
      col_skip(), # "MAPQ"
      col_double() #"Read_length"
  ))
  
  if (nrow(problems(tbl.df)) > 0) {
      flog.fatal(paste0("Parsing errors: "), problems(tbl.df), capture = TRUE)
      stop("Error when parsing the file ", fileCur, ", see errors above")
  }
  
  # Do it like this as different readr versions name the column differently: X1 vs X3
  colnames(tbl.df) = "Read_length"
  
  tbl.df = tbl.df %>%
      dplyr::filter(Read_length < par.l$cutoff, Read_length > 0) %>%
      mutate(Read_length = abs(Read_length))

  # make a table out of it to save space
  results.l[[fileCur]] = table(tbl.df)
  
  title = splitStringInMultipleLines(paste0(file_path_sans_ext(basename(fileCur))), 50)
 
  cap <- ggplot(tbl.df, aes(x = Read_length)) +
    geom_histogram(aes(y = ..count..),  color = par.l$ggplot_hist.col, binwidth = par.l$ggplot_binwidth) +
    xlab("Fragment length (in bp)") + ylab("Abundance") +
    ggtitle(title) + 
    scale_x_continuous(breaks = seq(0, par.l$cutoff, par.l$ggplot_maxBreaks), labels = seq(0,par.l$cutoff, par.l$ggplot_maxBreaks), limits = c(0, par.l$cutoff)) +
    theme(axis.text.x = element_text(color = par.l$ggplot_axis.col, size = par.l$ggplot_axis.text.x),
          axis.text.y = element_text(color = par.l$ggplot_axis.col, size = par.l$ggplot_axis.text.y),
          axis.title.x = element_text(colour = par.l$ggplot_axis.col, size = par.l$ggplot_axis.title.size, margin = margin(25,0,0,0)),
          axis.title.y = element_text(colour = par.l$ggplot_axis.col, size = par.l$ggplot_axis.title.size, margin = margin(0,25,0,0)),
          axis.line.x = element_line(color = par.l$ggplot_axis.col), axis.line.y = element_line(color = par.l$ggplot_axis.col),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          panel.background = element_blank(),
          legend.position = c(0.1, 0.9),
          legend.justification = "center",
          legend.title = element_blank(),
          plot.title = element_text(hjust =  0.5)
          )
  
  print(cap)

  
}


dev.off()

saveRDS(results.l, par.l$file_outputRData)

.printExecutionTime(start.time)

flog.info("Session info: ", sessionInfo(), capture = TRUE)
