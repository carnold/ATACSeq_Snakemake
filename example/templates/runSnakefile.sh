#!/bin/bash

# Cluster-specific settings, customized for the EMBL SLURM cluster.
# Important: !! Modifications here are only needed if NEITHER conda NOR Singularity is used (see below) !!
# !! Do not modify unless you know what you do!!
# !! Ignore the lines with comments below unless you have a different setup, ask Christian for advice

# Unload all modules and load the necessary ones for the pipeline
# One exception might be R, which might have to be loaded depending on whether it is preloaded or not in the .profile or .bashrc
# module purge
# module load GCCcore ncurses BEDTools SAMtools R-bundle-Bioconductor/3.5-foss-2016b-R-3.4.0 Autoconf FastQC Trimmomatic snakemake MACS2 deepTools

########################
# PATHS AND PARAMETERS #
########################

# All parameters and paths are defined here:
. "./params.sh"




########################
# RUN AUTOMATED SCRIPT #
########################
. "/g/scb/zaugg/zaugg_shared/scripts/Christian/src/Snakemake/runSnakemakeWrapper.sh"
