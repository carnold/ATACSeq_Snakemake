################################################
################################################
# CONFIGURATION FILE FOR THE ATAC-SEQ PIPELINE #
################################################
################################################

# This format allows comments and is therefore easier to work with than the json format we used before.
# Quotation marks are optional for strings. Make sure to put ": " (that is, hyphen space) as separator


#####################
# IMPORTANT CHANGES #
#####################

# See the parameter doRSSAdjustment below for which we recently changed the default choice.

##################
# SECTION output #
##################
output:

  # STRING. Absolute path to the output directory. Will be created if not yet present.
  outdir: "/path/to/output"

  # BOOLEAN. “true” or “false”. Default “true”. Should the pipeline use MACS2 to call peaks? If yes, peaks will be called in 3 different flavors (ENCODE, stringent, non-stringent). See the section “peakCalling” in the configuration file for details.
  doPeakCalling: true

  # BOOLEAN. “true” or “false”. Default “true”. Quantify reads per peak for each BAM file and each generated consensus peak file? This can then be used for downstream analsses such as diffTF or GRaNIE. if set to true, read counts are produced for each (!) consensus peak that is generated.
  countReadsPeak: true

  # BOOLEAN. “true” or “false”. Default “false”. Should the pipeline merge all replicate files and do all downstream analysis based on the merged files? If set to true, the final BAM files from all replciates are kept, and in addition, the merged BAM fiels are produced. If set to true, for each individual as specified in the sample table, all samples belonging to this individual ID will be merged to one file after all post-processing. This makes only sense if you have more than 1 replicate per individual as indicated in the sample table. Peak calling, consensus peaks, etc will then all be based on the merged files.
  alsoMergeReplicates: false

  # BOOLEAN. "true" or "false". Default "false". Should peaks be annotated to find out which genomic regions they overlap with?
  annotatePeaks: true

  # BOOLEAN. “true” or “false”. Default “true”. Should the pipeline produce coverage files and diagnostic plots? If set to true, coverage files for the final BAM files in bigwig and bedgraph.gz format will be produced as well as a coverage plot using deepTools.
  generateCoverageFiles: false

  #
  # The following options are deprecated, incomplete or not fully supported anymore. If you need them, contact Christian
  #

  # BOOLEAN. “true” or “false”. Default “false”. Should GC bias be assessed and corrected for the final BAM files in addition to the non-corrected ones? If set to true, the GC bias will be assessed and corrected using deepTools. Additionally, all downstream steps of the pipeline will be done for both GC-corrected and original files (including peak calling, PCA, coverage, etc). This greatly increases running time of th pipeline and essentially doubles the number of files. We recommend setting this to false unless the GC bias is important for downstream analyses
  # !!! Important: Leave at false for now, I have to fix an issue in the Snakefile that currently causes an error !!
  correctGCBias: false

  # BOOLEAN. “true” or “false”. Default “false”. Should base qualities be recalibrated using GATK to detect and correct systematic errors in base quality scores? This step can be time-consuming and needs the following other parameters: GATK_jar, knownSNPs, knownIndels. I recommend turning this off for now.
  doBaseRecalibration: false

  # BOOLEAN. "true" or "false". Default "false". Do peak calling also, in addition to the "stringent" and "non-stringent" flavour, according to ENCODE guidelines (as of 2018, we so far rarely used them). This is now deprecated and discouraged to use. For now, leave at false or talk to Christian if you want to use them.
  encodePeaks: false

  # BOOLEAN. “true” or “false”. Default “false”. Not implemented yet.
  doIDR: false


###################
# SECTION general #
###################
general:

  # INTEGER. Default 12. Maximum number of cores per rule. For local computation, the minimum of this value and the --cores parameter will define the number of CPUs per rule, while in a cluster setting, the minimum of this value and the number of cores on the node the jobs runs is used.
  maxCoresPerRule: 12

###################
# SECTION samples #
###################
samples:

  # STRING. No default. Absolute path to the sample summary file. See section 2.4 for details.
  summaryFile: "/path/to/samples.csv"

  # BOOLEAN. “true” or “false”. Default “true”. Paired-end data?
  pairedEnd: true

  # STRING. "ATACseq" or "ChIPseq". Only data type specific steps are executed. Currently, if set to ChIP-Seq, ATAC-seq specific steps like RSS adjustment are not executed, and peak calling may include control samples if the sample table specifies this.
  dataType: "ATACseq"

###########################
# SECTION additionalInput #
###########################
additionalInput:

  # STRING. Absolute path to the adapters file for Trimmomatic in fasta format. Default “/g/scb2/zaugg/zaugg_shared/Programs/Trimmomatic-0.33/adapters/NexteraPE-PE.fa”. There is usually no need to change this unless for your experiment, this adapter file is not suited.
  trimmomatic_adapters: "/g/zaugg/zaugg_shared/Programs/Trimmomatic-0.33/adapters/NexteraPE-PE.fa"

  # STRING. Absolute path to a BED file that contains the genomic regions that should be filtered from the peaks. The default depends on the genome assembly, see the templates for details. Only needed if doPeakCalling is set to true.
  blacklistRegions: "/g/zaugg/zaugg_shared/annotations/hg19/blacklisted/hg19-blacklist.v2.bed"

  # STRING. Absolute path to a database of known polymorphic sites (SNPs and Indels, respectively). This is only needed if (1) doBaseRecalibration is set to true and (2) the genome is either hg19 or hg38 and ignored otherwise. The default depends on the genome assembly, see the templates for details. Supported formats from GATK: BCF2, BEAGLE, BED, BEDTABLE, EXAMPLEBINARY, GELITEXT, RAWHAPMAP, REFSEQ, SAMPILEUP, SAMREAD, TABLE, VCF, VCF3.
  knownSNPs: "/g/zaugg/zaugg_shared/annotations/hg19/GATK_bundle/dbsnp_138.hg19.vcf.gz"
  knownIndels: "/g/zaugg/zaugg_shared/annotations/hg19/GATK_bundle/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf.gz"

  # STRING. The default depends on the genome assembly, see the templates for details. Absolute path to the reference genome in fasta and 2bit format, respectively, both of which have to correspond to the same genome assembly version as used for the alignment as well as the database of polymorphic sites (knownSNPs and knownIndels, if applicable).
  refGenome_fasta: "/g/zaugg/zaugg_shared/annotations/hg19/GATK_bundle/ucsc.hg19.onlyRefChr.fasta"
  refGenome_2bit: "/g/zaugg/zaugg_shared/annotations/hg19/GATK_bundle/ucsc.hg19.2bit"

  # STRING. The default depends on the genome assembly, see the templates for details. Absolute path to an genome annotation file in GTF format.
  annotationGTF: "/g/zaugg/zaugg_shared/annotations/hg19/Gencode_v19/gencode.v19.annotation.gtf"


#######################
# SECTION executables #
#######################

# If run with Singularity, this section can be ignored

executables:

  # STRING. Default “java”. Name of the executable for Java. Java version must be at least 1.8! Only needed if doBaseRecalibration is set to true
  java: "java"

  # STRING. Default "/g/scb2/zaugg/carnold/Projects/AtacSeq/src/tools/GenomeAnalysisTK.jar". Absolute path to a JAR file for the GATK suite. Only needed if doBaseRecalibration is set to true
  # NOT NEEDED and can be ignored unless  correctGCBias: true
  GATK_jar: "/g/zaugg/carnold/Projects/AtacSeq/src/tools/GenomeAnalysisTK.jar"

########################
# SECTION trimming #
########################
trimming:

  # STRING. Default "1:30:4:5:true". ILLUMINACLIP value. See trimmomatic manual
  trimmomatic_ILLUMINACLIP: "1:30:4:1:true"

  # INTEGER. Default 3. TRAILING value. See trimmomatic manual
  trimmomatic_trailing: 3

  # INTEGER. Default 20. MINLEN value. See trimmomatic manual
  trimmomatic_minlen: 20

  # STRING. Default "phred33". Phred type. See trimmomatic manual. The "-" is added automatically by the Snakemake pipeline.
  #trimmomatic_phredType: "phred33"

#####################
# SECTION align #
#####################
align:

  # STRING. Default "--very-sensitive". Sensitivity. Leave empty for the default sensitivity. See bowtie2 manual.
  bowtie2_sensitivity: "--very-sensitive"

  # INTEGER. Default 2000. Value for parameter X. See bowtie2 manual.
  bowtie2_maxFragmentLength: 2000

  # STRING. Default "/g/scb/zaugg/zaugg_shared/annotations/hg19/referenceGenome/Bowtie2/hg19". Absolute path to the reference genome + the prefix of the file with the index. In the default case, we use those files within /g/scb/zaugg/zaugg_shared/annotations/hg19/referenceGenome/Bowtie2 that start with hg19. See bowtie2 manual for more details.
  bowtie2_refGenome: "/g/zaugg/zaugg_shared/annotations/hg19/referenceGenome/Bowtie2/hg19"

  # STRING. Default “hg19”. Reference genome assembly version. Must match the one used by the alignment program.
  assemblyVersion: "hg19"


#########################
# SECTION postalign #
#########################
postalign:

  # INTEGER. Default 10. Minimum MAPQ score. Reads with a lower MAPQ quality will be removed during the post-alignment processing.
  minMAPQscore: 10

  # STRING. Default “LENIENT”. Value of the VALIDATION_STRINGENCY from SortSam (Picard tools). See the manual for details.
  ValidationStringencySortSam: "LENIENT"

  # STRING. Default “SILENT”. Value of the VALIDATION_STRINGENCY from MarkDuplicates (Picard tools). See the manual for details.
  ValidationStringencyMarkDuplicates: "SILENT"

  # STRING. Defauled “ID”. Used for filtering reads. Relates to the one letter abbreviations for CIGAR strings such as I for insertion and D for deletion.  Specify all the one letter abbreviations in the CIGAR string of a read here that should be filtered. "ID" keeps a read only if the CIGAR string does not contain the letters "I" and "D" (e.g., only M for example)
  CIGAR: "ID"

  #  BOOLEAN. “true” or “false”. Default “false”. Adjust the read start sites (RSS). Tn5 transposase has been shown to bind as a dimer and inserts two adaptors into accessible DNA locations separated by 9 bp. Therefore, for downstream analysis, such as peak-calling and footprinting, all reads in input bamfile need to be shifted. Many downstream tools now have a dedicated paameter for this so that it does not need to be done on the BAM files directly, as it is time-consuming and error-prone due to the read shifts and the possibility to go beyond the chromosome boundaries in edge cases. Thus, the default is now to NOT do this unless specified otherwise. If you want to adjust the RSS, see also the parameters adjustRSS_forward and adjustRSS_reverse below.
  doRSSAdjustment: false

  # INTEGER. Default 4 and -5, respectively. Adjustment of the read start positions on the forward and reverse strand and should be a positive or negative number, respectively. See the Buenrostro paper for details. FOR DATA TYPE ATAC-SEQ ONLY
  adjustRSS_forward: 4
  adjustRSS_reverse: 5

#######################
# SECTION scripts #
#######################

# NOTE: There is usually no need to modify the parameters here.
scripts:
  # INTEGER. Default 4000. The region size in bp that specifies what is considered within a TSS. The STATS script does a TSS enrichment test to test whether or not ATAC-Seq reads are primarily located within annotated TSS as opposed to outside of TSS regions. A value of 4000 means the region from -2kb up to +2kb of annotated TSS.
  STATS_script_withinThr: 4000

  # INTEGER. Default 1000. The size of the region adjacent to the within TSS region that is considered outside of a TSS. A value of 1000 therefore denotes the 1kb region up- and downstream of the within TSS region (from -3 to -2kb upstream and from +2 to +3 kb downstream of annotated TSS.)
  STATS_script_outsideThr: 1000

  # STRING. Default "protein_coding". Gene type to keep / do the analayses for. Allowed are gene types as specified by GENCODE. The default is "protein_coding"
  STATS_script_geneTypesToKeep: "protein_coding"

  # INTEGER. Default 600. Fragment length cutoff. All reads with a fragment length less than this value will be filtered for the purpose of this script.
  FL_distr_script_cutoff: 600

###########################
# SECTION peakCalling #
###########################
peakCalling:

  # STRING. Default "--nolambda –nomodel". Peak calling model for non-stringent peak calling.
  modelNonStringent: "--nolambda --nomodel"

  # STRING. Default "—nomodel”. Peak calling model for stringent peak calling.
  modelStringent: "--nomodel"

  # NUMERIC [0, 1]. Default 0.01. Minimum q-value threshold for stringent peak calling.
  modelStringent_minQValue: 0.01

  # NUMERIC [0, 1]. Default 0.1. Minimum q-value threshold for non-stringent peak calling.
  modelNonStringent_minQValue: 0.1

  # INTEGER. Default 10000. Value for slocal parameter for non-stringent peak calling.
  modelNonStringent_slocal: 10000

  # the enxt 3 options are only relevant if encodePeaks=true. ignored otherwise

  # NUMERIC [0, 1]. Default 0.1. p-value threshold for ENCODE peak calling.
  Encode_pValThreshold: 0.1

  # STRING. Default "--nomodel --shift -75 --extsize 150 --broad --keep-dup all". Model for Encode peak calling (broad and gapped mode)
  Encode_modelBroadAndGapped: "--nomodel --shift -75 --extsize 150 --broad --keep-dup all"

  # STRING. Default "--nomodel --shift -75 --extsize 150 -B --SPMR --keep-dup all –call-summits". Model for Encode peak calling (narrow mode)
  Encode_modelNarrow: "--nomodel --shift -75 --extsize 150 -B --SPMR --keep-dup all --call-summits"

#####################
# SECTION deepTools #
#####################
deepTools:

  # INTEGER. Read length. Default 100. Either 50, 100, 150, or 200. Based on this value, we provide automated estimates of the effective genome size as used in deepTools. If your read length is not one of them, approximate by rounding to the closest of the available options
  readLength: 100

  # STRING. Default “RPGC”. Possible choices: RPKM, CPM, BPM, RPGC, None. See https://deeptools.readthedocs.io/en/develop/content/tools/bamCoverage.html
  bamCoverage_normalizationCoverage: "RPGC"

  # INTEGER. Default 10. Size of the bins, in bases
  bamCoverage_binSize: 10

  # BOOLEAN. Default true. If the data is pair ended a read will by default be extended to match its pair.
  bamCoverage_extendReads: true

  # STRING. Default "--extendReads  --centerReads". Additional options that are supported by bamCoverage. Note that the "--" or "-" has to be present here.
  bamCoverage_otherOptions: "--centerReads"



#########################
# SECTION featureCounts #
#########################
featureCounts:

  # BOOLEAN. “true” or “false”. Default "false". Should read (pairs) be counted once for each gene they overlap with? If set to false, the default, read (pairs) overlapping with more than one meta-feature (=peak) will not be counted. Setting this to "true" may lead to artifacts because a read that overlaps multiple features will be counted once for each overlapping feature.
  countMultiOverlaps: false


###################
# SECTION multiqc #
###################
multiqc:

  # STRING. Default "/g/zaugg/carnold/Projects/AtacSeq/src/config/multiqc_config.yaml". Absolute path to the multiqc configuration file
  path_configFile: "../src/config/multiqc_config.yaml"
