#!/bin/bash

########################
# PATHS AND PARAMETERS #
########################

# All parameters and paths are defined here:
. "./params.sh"


########################
# RUN AUTOMATED SCRIPT #
########################
. "../src/wrapper/runSnakemakeWrapper.sh"
